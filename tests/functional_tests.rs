use std::process::Command;

static WITHOUT_ARGS_OUTPUT: &'static str = "error: The following required arguments were not provided:\n    <input>\n\nUSAGE:\n    wkdr [FLAGS] [OPTIONS] <input>\n\nFor more information try --help\n";
static SEARCH_ONE_WORD_OUTPUT: &'static str = "==================================================\nSearching for \"miskolc\" in \"en\" ...\n==================================================\n\u{1b}[38;2;0;152;229mQ102397\u{1b}[0m \u{1b}[38;2;0;200;0mMiskolc\u{1b}[0m city in Hungary \u{1b}[38;2;100;100;100m(No aliases)\u{1b}[0m \n\u{1b}[38;2;0;152;229mQ14630137\u{1b}[0m \u{1b}[38;2;0;200;0mMiskolc District\u{1b}[0m district of Hungary \u{1b}[38;2;100;100;100m(No aliases)\u{1b}[0m \n\u{1b}[38;2;0;152;229mQ729786\u{1b}[0m \u{1b}[38;2;0;200;0mUniversity of Miskolc\u{1b}[0m university \u{1b}[38;2;190;40;30mMiskolci Egyetem\u{1b}[0m, \n\u{1b}[38;2;0;152;229mQ960856\u{1b}[0m \u{1b}[38;2;0;200;0mMiskolc  Subregion\u{1b}[0m subregion of Hungary \u{1b}[38;2;100;100;100m(No aliases)\u{1b}[0m \n\u{1b}[38;2;0;152;229mQ854118\u{1b}[0m \u{1b}[38;2;0;200;0mMiskolc Tiszai railway station\u{1b}[0m railway station \u{1b}[38;2;100;100;100m(No aliases)\u{1b}[0m \n\u{1b}[38;2;0;152;229mQ627958\u{1b}[0m \u{1b}[38;2;0;200;0mMiskolci VSC\u{1b}[0m sports club in Hungary \u{1b}[38;2;100;100;100m(No aliases)\u{1b}[0m \n\u{1b}[38;2;0;152;229mQ928182\u{1b}[0m \u{1b}[38;2;0;200;0mMiskolci JJSE\u{1b}[0m ice hockey team \u{1b}[38;2;100;100;100m(No aliases)\u{1b}[0m \n==================================================\n";
static SEARCH_MULTIPLE_WORDS_OUTPUT: &'static str = "==================================================\nSearching for \"Csík Zenekar\" in \"en\" ...\n==================================================\n\u{1b}[38;2;0;152;229mQ686025\u{1b}[0m \u{1b}[38;2;0;200;0mCsík zenekar\u{1b}[0m band \u{1b}[38;2;100;100;100m(No aliases)\u{1b}[0m \n==================================================\n";

#[cfg(test)]
mod functional_tests {
    use Command;
    use WITHOUT_ARGS_OUTPUT;
    use SEARCH_ONE_WORD_OUTPUT;
    use SEARCH_MULTIPLE_WORDS_OUTPUT;

    #[test]
    fn call_wkdr_without_arguments() {
        let output = Command::new("./target/debug/wkdr")
            .output()
            .expect("error");

        assert_eq!(String::from_utf8_lossy(&output.stderr), WITHOUT_ARGS_OUTPUT);
    }

    #[test]
    fn search_one_word() {
        let output = Command::new("./target/debug/wkdr")
            .arg("miskolc")
            .output()
            .expect("error");

        assert_eq!(String::from_utf8_lossy(&output.stdout), SEARCH_ONE_WORD_OUTPUT);
    }

    #[test]
    fn search_multiple_words() {
        let output = Command::new("./target/debug/wkdr")
            .arg("Csík Zenekar")
            .output()
            .expect("error");

        assert_eq!(String::from_utf8_lossy(&output.stdout), SEARCH_MULTIPLE_WORDS_OUTPUT);
    }
}
