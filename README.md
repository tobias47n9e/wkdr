# Wikidata Rust CLI (wkdr)

A command line tool to view and query information from Wikidata.

## Installation

```bash
cargo install wkdr
```

## Usage

```bash
wkdr -h
```

### Searching

Any string that is not a valid entity ID will be searched:

```bash
wkdr Lviv
```

Use quotes to search for mulitple terms:

```bash
wkdr "zika virus"
```

Valid entity IDs (items or properties) display a summary. This
command will display all the data about Tirana:

```bash
wkdr Q19689
```

And this will show information about the property "Polish cultural heritage
register number":

```bash
wkdr P3424
```

## Contributing

This tool and the community around it are part of the Wikimedia movement.
Everybody is welcome to contribute. Plase refer to this guide
https://www.mediawiki.org/wiki/Code_of_Conduct
before contributing.

## License

GNU General Public License, Version 2 or later (GPL-2.0+)

## Repository / Issue tracker

https://gitlab.com/tobias47n9e/wkdr

## Crates.io

https://crates.io/crates/wkdr

## Dedication

Dedicated to Krzysztof Machocki. We will remember Krzysztof with his
contagious enthusiasm and dedication, for his vast encyclopedic knowledge
and wit, for his faith in how Wikipedia makes the world better, both online
and offline. We lose him but let us not lose the faith he shared with the
world. The memory of him remains and his Wikipedia legacy will continue
to help the world.

https://meta.wikimedia.org/wiki/CEE/Newsletter/January_2018/Contents/From_the_team
