// input.rs
//
// Copyright © 2018
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

use CliError;
use wikibase;

/// Splits comma separated ids and returns a vector of Strings
fn comma_separated_ids_as_vector(comma_separated_ids: String) -> Vec<String> {
    comma_separated_ids.split(",").map(|entity| entity.to_string()).collect()
}

/// Are Wikibase Ids valid
fn all_ids_valid(entities: &Vec<String>) -> bool {
    for entity in entities {
        match wikibase::validate::parse_wikibase_entity_type(&entity) {
            Ok(_) => {},
            Err(_) => return false
        };
    }

    true
}

/// Wikibase entities from input
///
/// Takes a input and returns a vector of IDs as Strings.
pub fn entities_from_input(input: &str) -> Result<Vec<String>, CliError> {
    let entities = comma_separated_ids_as_vector(input.to_string());

    match all_ids_valid(&entities) {
        true => Ok(entities),
        false => Err(CliError::Input("Not all entities are valid Wikibase Ids".to_string()))
    }
}
