// main.rs
//
// Copyright © 2018
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

extern crate clap;
extern crate ansi_term;
extern crate wikibase;

mod input;
mod printing;

use clap::{Arg, App, ArgMatches};
use std::str;
use ansi_term::Colour::RGB;
use wikibase::{Configuration, SearchResults, SnakType, Value};
use wikibase::query::{EntityQuery, SearchQuery};

/// Prints the summary of a Wikibase entity (item or property)
fn print_entity_summary(entity: &wikibase::Entity, query: &EntityQuery) {
    let mut alias_string = "".to_string();

    println!("=====================================");

    match entity.label_in_locale(&query.lang()) {
        Some(value) => {
            print!("{}", RGB(0, 152, 229).paint(value.to_string()));
        }
        None => {
            if *entity.missing() == true {
                print!("{}", RGB(100, 100, 100).paint("Entity missing"));
            } else {
                print!("{}", RGB(100, 100, 100).paint("Label missing"));
            }
        }
    };

    print!(" ({})", entity.id());

    match entity.description_in_locale(&query.lang()) {
        Some(value) => print!(" - {}", value),
        None => {}
    };

    print!("\n");

    if *entity.missing() == true {
        println!("=====================================");
        return
    }

    for alias in entity.aliases().iter() {
        if alias.language() == query.lang() {
            for (index, value) in alias.values().iter().enumerate() {
                alias_string.push_str(&format!("{}", value));

                if (index + 1) != alias.values().len() {
                    alias_string.push_str(", ");
                }
            }
        }
    }

    if alias_string.len() > 0 {
        println!("Aliases: {}", alias_string);
    }

    println!("=====================================");

    if entity.claims().len() > 0 {
        for claim in entity.claims().iter() {
            let snak = claim.main_snak();

            print!("{} -> ", snak.property().to_string());

            match snak.snak_type() {
                &SnakType::UnknownValue => {
                    print!("Unknown / Some value\n");
                    continue
                }
                &SnakType::NoValue => {
                    print!("No value\n");
                    continue
                }
                &SnakType::Value => {}
            };

            let data_value = match snak.data_value() {
                &Some(ref value) => value,
                &None => return
            };

            match data_value.value() {
                &Value::Coordinate(ref value) => {
                    print!("{} °N ", RGB(200, 0, 0).paint(value.latitude().to_string()));
                    print!("{} °E", RGB(200, 0, 0).paint(value.longitude().to_string()));
                }
                &Value::Entity(ref value) => {
                    print!("{}", RGB(0, 152, 229).paint(value.id().to_string()));
                }
                &Value::Time(ref value) => {
                    print!("{}", value.time().to_string());
                }
                &Value::MonoLingual(ref value) => {
                    print!("{} ({})", value.text(), value.language())
                }
                &Value::Quantity(ref value) => {
                    print!("{} ", RGB(200, 0, 0).paint(value.amount().to_string()));
                    print!("{}", value.unit());
                }
                &Value::StringValue(ref value) => {
                    print!("{} ", RGB(0, 200, 0).paint(value.to_string()));

                    if snak.datatype() == "commonsMedia" {
                        print!("https://commons.wikimedia.org/wiki/File:{}", printing::spaces_to_underscores(value));
                    }
                }
            };

            print!("\n");
        }
    } else {
        println!("{}", RGB(100, 100, 100).paint("No statements"));
    }

    println!("=====================================");
}

/// Runs a query for a single entity
///
/// Takes a vector of entities and converts it into a query. If the query
/// is successful a result-table is printed.
fn query_entities(entities: Vec<String>, configuration: &Configuration) {
    let query = EntityQuery::new(entities, "en");
    println!("Looking up \"{:?}\" in \"{}\" ...", query.ids(), query.lang());
    println!("{}", &query.url(configuration));
    let json_result = wikibase::Entity::new_from_query(&query, &configuration);

    match json_result {
        Ok(value) => {
            print_entity_summary(&value, &query);
        }
        Err(e) => {
            println!("{:?}", e);
        }
    }
}

/// Print search result table
///
/// Takes a SearchResults struct and prints a table to the terminal.
fn print_search_result_table(results: SearchResults) {
    print!("==================================================\n");

    for result in results.results() {
        print!("{} ", RGB(0, 152, 229).paint(result.id()));
        print!("{} ", RGB(0, 200, 0).paint(result.label().value()));
        match result.description() {
            &Some(ref description) => print!("{} ", description.value()),
            &None => print!("{} ", RGB(100, 100, 100).paint("(No description)"))
        };

        match result.aliases() {
            &Some(ref aliases) => {
                for alias in aliases.values() {
                    print!("{}, ", RGB(190, 40, 30).paint(alias.to_string()));
                }
            }
            &None => print!("{} ", RGB(100, 100, 100).paint("(No aliases)"))
        };

        print!("\n");
    }

    print!("==================================================\n");
}

/// Search query from matches
///
/// Constructs a search query from the search input and the argument matches.
/// Any missing or invalid input should fallback to the defaults.
fn search_query_from_matches(input: &str, matches: &ArgMatches) -> SearchQuery {
    let lang = matches.value_of("lang").unwrap_or_else(|| {"en"});
    let type_value = matches.value_of("type").unwrap_or_else(|| {"item"});
    let entity_type = match type_value {
        "property" | "p" => wikibase::EntityType::Property,
        _ => wikibase::EntityType::Item,
    };
    let limit_input = matches.value_of("limit").unwrap_or_else(|| {""});
    let limit = match limit_input.parse::<u64>() {
        Ok(value) => value,
        Err(_) => 7
    };
    let search_lang = match matches.value_of("search-lang") {
        Some(value) => value,
        None => lang
    };

    SearchQuery::new(input, lang, search_lang, limit, entity_type)
}

/// Searches for a term in Wikibase and prints the results
fn search_wikibase(input: &str, matches: &ArgMatches, configuration: &Configuration) {
    let query = search_query_from_matches(&input, &matches);
    print!("==================================================\n");
    println!("Searching for \"{}\" in \"{}\" ...", input, query.lang());

    match SearchResults::new_from_query(&query, &configuration) {
        Ok(value) => print_search_result_table(value),
        Err(error) => println!("{:?}", error)
    };
}

#[derive(Debug)]
pub enum CliError {
    Input(String),
}

/// Process input
///
/// Input is interpreted as item, property or search string.
/// Without any modifiers a summary of the information is printed.
fn process_input(input: &str, matches: &ArgMatches, configuration: &Configuration) {
    match input::entities_from_input(&input) {
        Ok(values) => {
            query_entities(values, &configuration);
        }
        Err(_) => {
            search_wikibase(&input, &matches, &configuration);
        }
    };
}

/// Main function: Set up matches and parse them
///
/// Parses the input from the terminal and calls the function that processes
/// the input.
fn main() {
    let input = "input";
    let limit = "limit";
    let entity_type = "type";
    let request_url = "request-url";
    let configuration = wikibase::Configuration::new("wkdr/0.2.0").unwrap();

    let matches = App::new("wkdr")
        .version("0.2.0")
        .about("View and query Wikidata")
        .arg(Arg::with_name(input)
            .help("Item, property or search string")
            .required(true)
            .index(1))
        .arg(Arg::with_name("lang")
            .short("l")
            .long("lang")
            .value_name("Language identifier")
            .help("Language or locale of returned information. By default search and return value.")
            .takes_value(true))
        .arg(Arg::with_name("search-lang")
            .long("search-lang")
            .value_name("Language used for search")
            .help("Specifies the language used for searching, when different from display language.")
            .takes_value(true))
        .arg(Arg::with_name(entity_type)
            .short("t")
            .long(entity_type)
            .value_name("Entity type to be searched")
            .help("Either item (default) or property")
            .takes_value(true))
        .arg(Arg::with_name(limit)
            .short("n")
            .long(limit)
            .value_name("Number of results")
            .help("Number of returned results")
            .takes_value(true))
        .arg(Arg::with_name(request_url)
            .long(request_url)
            .value_name("Also print the request URL")
            .help("The URL of the request is printed at the end")
            .takes_value(false))
        .get_matches();

    // Input machtes. Load summary of item or search.
    match matches.value_of(input) {
        Some(value) => {
            process_input(&value, &matches, &configuration);
            return
        },
        None => {}
    }
}
